<?php
class Goodbye {
  const LEAVING_MESSAGE = "Hello There!";
  public function byebye() {
    echo self::LEAVING_MESSAGE;
  }
}

$goodbye = new Goodbye();
$goodbye->byebye();

// Constants are used to store and use constant values inside a class   

?>