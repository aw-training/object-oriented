<?php

class fruit
{
	public $name;
	public $color;
	function __construct($name,$color) {
		$this->name = $name;
		$this->color = $color;
	}
	function __destruct() {
		echo "Destructor says:<br>The Fruit is {$this->name} and the other color is {$this->color}.";
	}
}

$apple = new fruit("apple","red");

?>